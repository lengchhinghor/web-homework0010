import React,{useState,useEffect} from "react";
import { Col, Container, Row,Form,Button, Table , Image} from "react-bootstrap";
import { useParams } from "react-router";
import { fetchAuthorById, postAuthor, uploadImage, updateAuthorById, fetchAuthor, deleteAuthor } from "../services/authur_service";
import { useHistory } from "react-router";
function Authur() {

  const [author, setAuthor] = useState([])
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [imageURL, setImageURL] = useState('/image/default-file.png')
  const [imageFile, setImageFile] = useState(null)
  const {id} = useParams()
  const history = useHistory()

  useEffect(() => {
    const fetch = async () => {
        let author = await fetchAuthor() ;
        console.log(author)
        setAuthor(author);
      };
      fetch();
      
    if(id){
      fetchAuthorById(id).then(author=>{
          console.log(author.name);
        setName(author.name)
        setEmail(author.email)
        setImageURL(author.image)
      })
    }


  }, [])


  const onAdd = async(e)=>{
      e.preventDefault()
      let author = {
          name,email
      }
      if(imageFile){
         let url = await uploadImage(imageFile)
         author.image = url
      }
      postAuthor(author).then(message=>alert(message))
  }

  const onUpdate = async(e)=>{
    e.preventDefault()
    let author = {
        name,email
    }
    if(imageFile){
       let url = await uploadImage(imageFile)
       author.image = url
    }
    updateAuthorById(id,author).then(message=>alert(message))
}

const onDelete = (id)=>{
    deleteAuthor(id).then((message)=>{

      let newAuthor = author.filter((author)=>author._id !== id)
      setAuthor(newAuthor)

      alert(message)  
    }).catch(e=>console.log(e))
  }

  return (
    <Container>
      <h1 className="my-2">{id?"Update Author":"Add Author"}</h1>
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group controlId="name">
              <Form.Label> Name</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Username" 
                value={name}
                onChange={(e)=>setName(e.target.value)}
                />
              <Form.Text className="text-muted">
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="email">
              <Form.Label>Email</Form.Label>
              <Form.Control 
                as="textarea" 
                rows={4} 
                placeholder="Email" 
                value={email}
                onChange={(e)=>setEmail(e.target.value)}
              />
              
            </Form.Group>
            <Button 
                variant="primary" 
                type="submit"
                onClick={id? onUpdate:onAdd}
            >
              {id?'Save':'Add'}
            </Button>
          </Form>
        </Col>
        <Col md={4}>
            <img className="w-100" src={imageURL}/>
            <Form>
            <Form.Group>
                <Form.File 
                    id="img" 
                    label="Choose Image" 
                    onChange={(e)=>{
                        let url = URL.createObjectURL(e.target.files[0])
                        setImageFile(e.target.files[0])
                        setImageURL(url)
                    }}
                    />
            </Form.Group>
            </Form>
        </Col>
      </Row>
      <Row>
      <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Image</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                 {
                     author.map((item,index)=>(
                         <tr key={index}>
                             <td>{item._id}</td>
                             <td>{item.name}</td>
                             <td>{item.email ? item.email : "No discription found"}</td>
                             <td><Image src={item.image ? item.image : "image/default.jfif"} style={{ objectFit: "cover", height: "150px" }}></Image></td>
                             <td>
                             <Button 
                                    size="sm" 
                                    variant="warning"
                                    onClick={()=>{
                                    history.push('/update/authur/'+item._id)
                                    }}
                                >
                                    Edit
                                </Button>{" "}
                                <Button size="sm" variant="danger" onClick={()=>onDelete(item._id)}>
                                    Delete
                             </Button>
                             </td>
                         </tr>
                     ))
                 }
                </tbody>
                </Table>
      </Row>
    </Container>
  );
}

export default Authur;
