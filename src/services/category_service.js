import api from "../api/api"

export const fetchCategory = async () => {

    let response = await api.get('category')

    return response.data.data

}